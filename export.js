let MongoClient = require('mongodb').MongoClient;

var exec = require('child_process').exec;

// db connection string with databse from where you are going to export the record
let exportDbConnectionString = 'db-connection-string-will-go-here';

// db hosts in comma seperated format
let dbExportFromHosts = 'host1,host2'

// db hosts in comma seperated format
let dbImportToHosts = 'host1,host2'

let dbuser = 'user'
let dbpassword = 'pass' 

//records import to databse urls 
let dbName = 'databasename';

let collectionNames = [];
MongoClient.connect(exportDbConnectionString, function(err, client) {
    if(err){
    console.log("error while connecting");
    }
    console.log("connected");
    const db = client.db(dbName);

    db.listCollections().toArray((err, collections) => {
        
       console.log("Number of collections found in the "+dbName+" = "+collections.length);
        for(let collectionIndex = 0; collectionIndex<collections.length; collectionIndex ++){
         collectionNames.push(collections[collectionIndex].name)
       }
    
   //importDb();
    exportDb(dbExportFromHosts);
   client.close();
    });
});

function exportDb(dbExportFromHosts){
  
   for(let collection of collectionNames){
      cmd = `mongoexport --host `+dbExportFromHosts+` --ssl --username ${dbuser} --password ${dbpassword} --authenticationDatabase admin --db `+ dbName +` --collection `+ collection +` --type JSON --out `+ collection +`.json
      `
   let result = await Exec.exec(cmd);

   }
}

function importDb(){
   for(let collection of collectionNames){
     let cmd = `mongoimport --host ${dbImportToHosts} --ssl --username ${dbuser} --password ${dbpassword} --authenticationDatabase admin --db `+dbName+` --collection `+collection+` --type JSON --file `+collection+`.json`

   let result = await Exec.exec(cmd);
   }
}

 class Exec{
   static async exec(cmd) {
      let result = { success: true, logs: '' }
      return new Promise((resolve, reject) => {
          exec(cmd, (error, stdout, stderr) => {
              if (error) {
                  result.success = false;
                  result.logs = "Command:", cmd, "\nERROR Ocurred:", stderr;
              }
              result.logs += stdout + "\n" + stderr;
              resolve(result);
          });
      });
  }
 }